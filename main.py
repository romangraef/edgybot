import sys

import discord
import os

import daemon

smileys = [
    ':^)',
    ':^(',
]


def cat(path):
    with open(str(path)) as handle:
        return handle.read()


class MyClient(discord.Client):
    async def on_ready(self):
        print('[INFO] Logged in as')
        print('[INFO] ' + self.user.name)
        print('[INFO] ' + self.user.id)
        print('[INFO] ------')

    async def on_message(self, message: discord.Message):
        if message.author == self.user:
            return
        if message.content not in smileys:
            return
        print('[INFO] Edgymaster confirmed: %s(%s#%s): %s' % (
            message.author.id, message.author.name, message.author.discriminator, message.content))
        await self.send_message(message.channel, message.content)


client = MyClient()


class MyDaemon(daemon.Daemon):
    def run(self):
        client.run(cat('token.txt'))

import os

client.run(os.environ["TOKEN"])

